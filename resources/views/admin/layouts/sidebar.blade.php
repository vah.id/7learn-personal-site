<div class="card">
    <div class="card-header">Sidebar</div>
    <div class="card-body">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        <ul>
            <li><a href="{{ route('admin') }}">Dashboard</a></li>
            <li><a href="{{ route('viewListPortfolios') }}">Portfolios</a></li>
            <li><a href="{{ route('viewAboutMe') }}">About Me</a></li>
            <li><a href="{{ route('viewMessages') }}">Messages</a></li>
        </ul>
    </div>
</div>
